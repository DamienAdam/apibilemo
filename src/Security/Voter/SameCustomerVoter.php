<?php

namespace App\Security\Voter;

use App\Entity\Customer;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class SameCustomerVoter extends Voter
{
    const ACCESS_CUSTOMER_RESSOURCE = 'accessCustomerRessource';
    const ACCESS_CUSTOMER = 'accessCustomer';

    protected function supports($attribute, $subject)
    {
        return (in_array($attribute, ['ACCESS_CUSTOMER_RESSOURCE']) && $subject instanceof User) ||
            (in_array($attribute, ['ACCESS_CUSTOMER']) && $subject instanceof Customer);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var UserInterface $user */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'ACCESS_CUSTOMER_RESSOURCE':
                if ($user->getCustomer() === $subject->getCustomer()) return true;
                break;
            case 'ACCESS_CUSTOMER':
                if ($user->getCustomer() === $subject) return true;
                break;
        }

        return false;
    }
}
