<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

final class CreateUserDataPersister implements ContextAwareDataPersisterInterface
{
    private $entityManager;

    /**
     * @var UserInterface
     */
    private $loggedUser;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var RequestContext
     */
    private $request;

    public function __construct(EntityManagerInterface $entityManager, Security $security,
                                UserPasswordEncoderInterface $encoder, RequestContext $request)
    {
        $this->entityManager = $entityManager;
        $this->loggedUser = $security->getUser();
        $this->encoder = $encoder;
        $this->request = $request;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof User && $this->request->getMethod() === Request::METHOD_POST;
    }

    /**
     * @param User $data
     * @param array $context
     * @return User|object|void
     */
    public function persist($data, array $context = [])
    {
        $user = new User();
        $passHash = $this->encoder->encodePassword($user, $data->getPassword());
        $data->eraseCredentials();

        $user->setPassword($passHash);
        $user->setEmail($data->getEmail());
        $user->setCustomer($this->loggedUser->getCustomer());

        $this->entityManager->persist($user);
        $this->entityManager->flush();


        return $user;
    }

    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}