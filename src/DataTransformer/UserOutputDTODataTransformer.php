<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\DTO\UserOutputDTO;
use App\Entity\User;

class UserOutputDTODataTransformer implements DataTransformerInterface
{
    /**
     * @var User $object
     * @inheritDoc
     */
    public function transform($object, string $to, array $context = [])
    {
        $output = new UserOutputDTO();
        $output->email = $object->getEmail();
        $output->id = $object->getId();
        $output->customer = $object->getCustomer();

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return UserOutputDTO::class === $to && $data instanceof User;
    }
}