<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\CustomerAware;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\DTO\UserOutputDTO;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @CustomerAware(customerFieldName="customer_id")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity("email", message="Email already in use")
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "output"=UserOutputDTO::class
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={"user_create"}},
 *              "output"=UserOutputDTO::class
 *          }
 *      },
 *     itemOperations={
 *          "get"={
 *              "output"=UserOutputDTO::class,
 *              "security"="is_granted('ACCESS_CUSTOMER_RESSOURCE', object)",
 *              "security_message"="Access denied: user should be from the same customer."
 *          },
 *          "put"={
 *              "denormalization_context"={"groups"={"user_put"}},
 *              "output"=UserOutputDTO::class,
 *              "security"="is_granted('ACCESS_CUSTOMER_RESSOURCE', object)",
 *              "security_message"="Access denied: user should be from the same customer."
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={"user_patch"}},
 *              "output"=UserOutputDTO::class,
 *              "security"="is_granted('ACCESS_CUSTOMER_RESSOURCE', object)",
 *              "security_message"="Access denied: user should be from the same customer."
 *          },
 *          "delete"={
 *              "output"=UserOutputDTO::class,
 *              "security"="is_granted('ACCESS_CUSTOMER_RESSOURCE', object)",
 *              "security_message"="Access denied: user should be from the same customer."
 *          }
 *      }
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Email is mandatory")
     * @Assert\Email(message="Invalid Email")
     * @Groups({"user_create", "user_put", "user_patch", "user_list"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Password is mandatory")
     * @Groups({"user_create"})
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }
}
